/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.dao;

import java.util.List;
import mx.com.buap.imagecontainercore.vo.ImgVO;

/**
 *
 * @author andres
 */
public interface ImgDAO {

    ImgVO getImg(String isbn) throws Exception;
    String[] loadImgs(List<ImgVO> listImg) throws Exception;
    
}
