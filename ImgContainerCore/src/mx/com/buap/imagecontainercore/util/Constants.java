/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.util;

import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author andres
 */
public class Constants {

    public final static int ISBN_13_LEN = 13;
    public final static int ISBN_10_LEN = 10;

    public final static String IMG_JPEG_CONTENT_TYPE = "image/jpeg";
    public final static String IMG_DIRECTORY = "C:/img/";
    public final static String IMG_PROCESSED_DIRECTORY = "C:/imgprocessed/";
    public final static String JPEG_EXTENSION = "JPEG";
    public final static String JPG_EXTENSION = "JPG";
    public final static String ISBN_13_PATTERN = "\\w{13}";
    public final static String ISBN_10_PATTERN = "\\w{10}";
    public final static String FULL_PATTERN =  ISBN_13_PATTERN+ "-" + ISBN_10_PATTERN + "\\.((JPG)|(JPEG))";
    public final static Pattern PATTERN_ISBN_13 = Pattern.compile(ISBN_13_PATTERN);
    public final static Pattern PATTERN_ISBN_10 = Pattern.compile(ISBN_10_PATTERN);
    public final static int BUFFER_SIZE = 1024;
    public final static int MAX_BLOCKS = 20;        
  
    public final static Map<String, String> MAP_RESULTS;
    
    static {
        
        MAP_RESULTS = new HashMap<String, String>(2);
        MAP_RESULTS.put("1", "Actualizada");
        MAP_RESULTS.put("2", "Insertada");
        
    }
    
}
