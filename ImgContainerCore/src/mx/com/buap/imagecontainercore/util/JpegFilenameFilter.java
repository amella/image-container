/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.util;

import java.io.File;
import java.io.FilenameFilter;
import static mx.com.buap.imagecontainercore.util.Constants.JPG_EXTENSION;
import static mx.com.buap.imagecontainercore.util.Constants.JPEG_EXTENSION;

/**
 *
 * @author andres
 */
public class JpegFilenameFilter implements FilenameFilter {

    public boolean accept(File dir, String name) {

        name = name.toUpperCase();

        return name.endsWith(JPG_EXTENSION) || name.endsWith(JPEG_EXTENSION);

    }

}
