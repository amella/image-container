/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.sql.ibatis;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mx.com.buap.imagecontainercore.dao.ImgDAO;
import mx.com.buap.imagecontainercore.vo.ImgVO;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ImageDAOImpl implements ImgDAO {

    private final static Logger LOG = Logger.getLogger(ImageDAOImpl.class);
    
    private SqlMapClient sqlMapClient;

    public void setSqlMapClient(SqlMapClient sqlMapClient) {
        this.sqlMapClient = sqlMapClient;
    }

    public ImgVO getImg(String isbn) throws SQLException  {
        
        LOG.info("Inicia getImg en ImageDAOImpl"); 
        
        ImgVO imgVOTmp = (ImgVO) this.sqlMapClient.queryForObject("getImage",isbn);
        
        LOG.info("Inicia getImg en ImageDAOImpl"); 
        
        return imgVOTmp;

    }

    public String[] loadImgs(List<ImgVO> listImg) throws SQLException {                        

        LOG.info("Inicia loadImgs en ImageDAOImpl"); 
        
        int len = listImg.size();
        
        String[] results = new String[len];
        
        for(int i = 0;i<len;i++) {

            ImgVO imgVO = listImg.get(i);
            
            Map<String, Object> map = new HashMap<String, Object>(3); 
            map.put("img", imgVO.getImg());            
            map.put("isbn13", imgVO.getIsbn13());            
            map.put("isbn10", imgVO.getIsbn10());   
            
            this.sqlMapClient.queryForObject("imginsup", map);

            results[i] = map.get("result").toString();

            map = null;

        }                       

        LOG.info("Finaliza loadImgs en ImageDAOImpl"); 
        
        return results;
        
    }

}
