/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.vo;

import com.mysql.jdbc.Blob;
import java.util.Date;

/**
 *
 * @author andres
 */
public class ImgVO {

    private String isbn13;
    private String isbn10;
    private Date fachaCreacion;
    private Date fechaActualizacion;
    private byte[] img;

    public Date getFachaCreacion() {
        return fachaCreacion;
    }

    public void setFachaCreacion(Date fachaCreacion) {
        this.fachaCreacion = fachaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public String getIsbn10() {
        return isbn10;
    }

    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

}
