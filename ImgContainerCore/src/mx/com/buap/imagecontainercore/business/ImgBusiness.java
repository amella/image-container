/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.business;

import java.io.OutputStream;
import mx.com.buap.imagecontainercore.vo.ImgVO;

/**
 *
 * @author andres
 */
public interface ImgBusiness {

    ImgVO getImg(String isbn) throws Exception;
    void writeTo(OutputStream outputStream, ImgVO imgVO) throws Exception;
    boolean validateIsbn(String isbn);
    void loadImgs() throws Exception;
}
