/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.buap.imagecontainercore.business;

import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.List;
import java.util.ArrayList;
import mx.com.buap.imagecontainercore.util.JpegFilenameFilter;
import java.io.FilenameFilter;
import java.io.File;
import java.io.OutputStream;
import mx.com.buap.imagecontainercore.dao.ImgDAO;
import mx.com.buap.imagecontainercore.vo.ImgVO;

import static mx.com.buap.imagecontainercore.util.Constants.ISBN_13_LEN;
import static mx.com.buap.imagecontainercore.util.Constants.ISBN_10_LEN;
import static mx.com.buap.imagecontainercore.util.Constants.IMG_DIRECTORY;
import static mx.com.buap.imagecontainercore.util.Constants.IMG_PROCESSED_DIRECTORY;
import static mx.com.buap.imagecontainercore.util.Constants.PATTERN_ISBN_13;
import static mx.com.buap.imagecontainercore.util.Constants.PATTERN_ISBN_10;
import static mx.com.buap.imagecontainercore.util.Constants.FULL_PATTERN;
import static mx.com.buap.imagecontainercore.util.Constants.BUFFER_SIZE;
import static mx.com.buap.imagecontainercore.util.Constants.MAX_BLOCKS;
import static java.lang.Math.round;
import static mx.com.buap.imagecontainercore.util.Constants.MAP_RESULTS;
/**
 *
 * @author andres
 */
public class ImgBusinessImpl implements ImgBusiness {

    private final static Logger LOG = Logger.getLogger(ImgBusinessImpl.class);
    
    private ImgDAO imgDAO;

    public void setImgDAO(ImgDAO imgDAO) {
        this.imgDAO = imgDAO;
    }

    public ImgVO getImg(String isbn) throws Exception {

        LOG.info("Inicia getImg en ImgBusinessImpl"); 
        
        ImgVO imgVOTmp = this.imgDAO.getImg(isbn);

        LOG.info("Inicia getImg en ImgBusinessImpl"); 
        
        return imgVOTmp;
        
    }

    public void writeTo(OutputStream outputStream, ImgVO imgVO) throws Exception {

        LOG.info("Inicia writeTo en ImgBusinessImpl"); 
        
        byte[] bytes = imgVO.getImg();
        int len = bytes.length;
        
        LOG.info("bytes a escribir en el outputstream : "+len);
        
        outputStream.write(bytes, 0, len);
        outputStream.close();

        LOG.info("Finaliza writeTo en ImgBusinessImpl"); 
        
    }

    public boolean validateIsbn(String isbn) {

        LOG.info("Inicia validateIsbn en ImgBusinessImpl");
        
        boolean valid = false;

        isbn = StringUtils.trimToNull(isbn);       
        
        if(StringUtils.isNotBlank(isbn)) {
          
               int isbnLength = isbn.length();
               valid = isbnLength == ISBN_13_LEN || isbnLength == ISBN_10_LEN;            
                         
               
        }
        
        LOG.info("isbn es valido : "+valid);
        
        LOG.info("Finaliza validateIsbn en ImgBusinessImpl");
        
        return valid;
    }

    private byte[] loadImg(File file) {

        LOG.info("Inicia loadImg en ImgBusinessImpl");
        
        FileInputStream fis = null;
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        byte[] buf = new byte[BUFFER_SIZE];
        int readNum = -1;

        LOG.info("file : "+file.getAbsolutePath());
        
        try {

            fis = new FileInputStream(file);
            bos = new ByteArrayOutputStream();
       
            while((readNum = fis.read(buf)) != -1) {

                bos.write(buf, 0, readNum);

            }

            bytes = bos.toByteArray();

        }catch(FileNotFoundException ex) {
            LOG.error(ex.getCause());   
            LOG.error(ex.getMessage());
            bytes = null;

        }catch(IOException ex) {
            LOG.error(ex.getCause());   
            LOG.error(ex.getMessage());
            bytes = null;

        }

        LOG.info("Finaliza loadImg en ImgBusinessImpl");
        
        return bytes;

    }

    private ImgVO getImgByFileName(String name) {

        LOG.info("Inicia getImgByFileName en ImgBusinessImpl");
        
            ImgVO imgV0 = null;

            String isbn = null;
           
            int beginIndex = 0;
            int endIndex = 0;

            Matcher matcher13 = null;
            Matcher matcher10 = null;

            name = name.toUpperCase();

            LOG.info("nombre de imagen: "+name);
            
            boolean matches = name.matches(FULL_PATTERN);

            if(matches) {

		matcher13 = PATTERN_ISBN_13.matcher(name);
		matcher10 = PATTERN_ISBN_10.matcher(name);

		if(matcher13.find() && matcher10.find()) {

                    imgV0 = new ImgVO();

                    beginIndex = matcher13.start();
                    endIndex = matcher13.end();

                    isbn = name.substring(beginIndex, endIndex);

                     LOG.info("estableciendo isbn 13 : "+isbn);
                    imgV0.setIsbn13(isbn);
                    
                    beginIndex = matcher10.start();
                    endIndex = matcher10.end();

                    isbn = name.substring(beginIndex, endIndex);
                     LOG.info("estableciendo isbn 10 : "+isbn);
                    imgV0.setIsbn10(isbn);
                     
		}

            }

            LOG.info("Finaliza getImgByFileName en ImgBusinessImpl");
            
            return imgV0;
            
    }

    private File[] getImageFiles() {

        LOG.info("Inicia getImageFiles en ImgBusinessImpl");
        
        File imageDirectory = new File(IMG_DIRECTORY);
        FilenameFilter jpegNameFilter =  new JpegFilenameFilter();
        File[] images = imageDirectory.listFiles(jpegNameFilter);

        LOG.info("Finaliza getImageFiles en ImgBusinessImpl");
        
        return images;
        
    }

    private List<ImgVO> getImages(File[] images) {
      
        LOG.info("Inicia getImages en ImgBusinessImpl");
        
        List<ImgVO> listImgs = new ArrayList<ImgVO>();
        byte[] bytes = null;

        for(File img:images) {

            String name = img.getName();

            ImgVO imgVO = this.getImgByFileName(name);

            if(imgVO!=null) {

                bytes = this.loadImg(img);

                if(bytes!= null) {
                    
                    LOG.info("Estableciendo bytes de imagen");
                    imgVO.setImg(bytes);
                    listImgs.add(imgVO);

                }
             
            }

        }
        
        LOG.info("Finaliza getImages en ImgBusinessImpl");
        
        return listImgs;

     }

    private void processBlock(File[] images, int j) throws Exception{

        LOG.info("Inicia processBlock en ImgBusinessImpl");
        
        int len = images.length-j;

        File[] imagesMaxBlock = new File[len];

        for(;j<images.length;j++) {
            imagesMaxBlock[j] = images[j];
        }

        this.doBatch(imagesMaxBlock);

        LOG.info("Finaliza processBlock en ImgBusinessImpl");
        
    }

    private int processBlock(File[] images, int i, int j) throws Exception{

        LOG.info("Inicia processBlock en ImgBusinessImpl");
        
        int blockEnd = (i+1)*MAX_BLOCKS-1;

        File[] imagesMaxBlock = new File[MAX_BLOCKS];

        for(;j<blockEnd;j++) {
            imagesMaxBlock[j] = images[j];
        }

        this.doBatch(imagesMaxBlock);

        LOG.info("Finaliza processBlock en ImgBusinessImpl");
        
        return j;
    }

    private void doBatch(File[] imagesMaxBlock) throws Exception {

        LOG.info("Inicia doBatch en ImgBusinessImpl");
        
        List<ImgVO> listImgs = this.getImages(imagesMaxBlock);
        
        if(listImgs!=null && listImgs.size()>0) {

            String[] results = this.imgDAO.loadImgs(listImgs);
            
            int len = listImgs.size();
        
            StringBuilder stringBuilder = new StringBuilder();
            
            for(int i =0;i<len;i++) {
            
               ImgVO imgVO = listImgs.get(i);
               
               stringBuilder.append("Imagen : ");
               stringBuilder.append(imgVO.getIsbn13());
               stringBuilder.append("-");
               stringBuilder.append(imgVO.getIsbn10());
               stringBuilder.append(" ");
               stringBuilder.append(MAP_RESULTS.get(results[i]));
                              
               System.out.println(stringBuilder.toString());
               
               stringBuilder.delete(0, stringBuilder.length());
               
            }

        }
        
        listImgs =  null;

        LOG.info("Finaliza doBatch en ImgBusinessImpl");
        
    }

    public void loadImgs() throws Exception {

        LOG.info("Inicia loadImgs en ImgBusinessImpl");
        
        File[] images = this.getImageFiles();

        int len = -1;
        
        if(images == null || (len = images.length) == 0) {        
            
            LOG.info("No se encontraron imagenes");
            return;
            
        }                

        LOG.info("Imagenes a insertar : "+len);
        
        int blocks = 1;
        int i;
        int j=0;
        
        if(len > MAX_BLOCKS) {
        
            blocks = round(((float)len)/((float)MAX_BLOCKS));

            LOG.info("Bloques por procesar :"+blocks);
            
            for( i = 0 ; i < blocks-1; i++) {

            j = this.processBlock(images, i, j);

            }

            if(i<blocks) {
                
                LOG.info("Procesando ultimo bloque");
                this.processBlock(images, j);

            }
            
        }else {
            
            LOG.info("Procesando bloque");
            this.processBlock(images, j);
        
        }
                            
        if(images.length > 0) {
            
            LOG.info("Mandando a mover archivos");
            this.delMovFiles(images);  
            
        }

        LOG.info("Finaliza loadImgs en ImgBusinessImpl");
        
    }

    private void delMovFiles(File[] images) {
        
        LOG.info("Inicia delMovFiles en ImgBusinessImpl");
        
        File toDirectory = new File(IMG_PROCESSED_DIRECTORY);        
        
        for(File img:images) {
        
            File file = new File(toDirectory, img.getName());                                                
            
            if(file.exists() && (file.delete()== false)) {
                
                System.out.println("No se pudo eliminar el archivo : "+file.getAbsolutePath());   
                
            }                        
            
            if(img.renameTo(file) == false) {
                
                System.out.println("No se pudo realizar el cambio de directorio pra la imagen : "+file.getAbsolutePath());
                
            }
            
        }
         
         LOG.info("Finaliza delMovFiles en ImgBusinessImpl");
        
    }
    
}
