package mx.com.buap.imagecontainer.controller;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.buap.imagecontainercore.business.ImgBusiness;
import mx.com.buap.imagecontainercore.vo.ImgVO;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import static mx.com.buap.imagecontainercore.util.Constants.IMG_JPEG_CONTENT_TYPE;
/**
 *
 * @author andres
 */
public class IndexController implements Controller {

    private final static Logger LOG = Logger.getLogger(IndexController.class);
    
    private ImgBusiness imgBusiness;

    public void setImgBusiness(ImgBusiness imgBusiness) {
        this.imgBusiness = imgBusiness;
    }

    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {

       LOG.info("Inicia handleRequest en IndexController"); 
        
       OutputStream outputStream = null;
       ImgVO imgVO = null;

       String isbn = hsr.getParameter("isbn");

       LOG.info("isbn de request: "+isbn);
       
       //avoid SQL injection
       boolean valid = this.imgBusiness.validateIsbn(isbn);
        
       LOG.info("isbn valido: "+ valid);
        
       if(valid) {                      

           imgVO = new ImgVO();           
           imgVO = this.imgBusiness.getImg(isbn);
           
           outputStream = hsr1.getOutputStream();
           hsr1.setContentType(IMG_JPEG_CONTENT_TYPE);
           
           this.imgBusiness.writeTo(outputStream, imgVO);

           LOG.info("Se manda imgVO al outputstream");
           
       }

       LOG.info("Finaliza handleRequest en IndexController"); 
       
       return null;

    }

}
